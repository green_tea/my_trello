<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\VerifyCsrfToken;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('client:')->group(function () {
    Route::name('users:')->prefix('users')->group(function () {
        Route::put('update_me', 'UserController@updateMe')
            ->name('update_me')
            ->middleware(VerifyCsrfToken::class);
        Route::get('get_me', 'UserController@getMe')->name('get_me');
        Route::put('update_my_avatar', 'UserController@updateMyAvatar')
            ->name('update_my_avatar')
            ->middleware(VerifyCsrfToken::class);
    });

    Route::name('desks:')->prefix('desks')->group(function () {
        Route::get('recently_viewed', 'DeskController@recentlyViewed')->name('recently_viewed');
    });
});
