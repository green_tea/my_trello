<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Models\Desk;

$factory->define(Desk::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'viewed_at' => $faker->dateTime,
    ];
});
