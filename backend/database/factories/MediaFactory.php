<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Models\Media;

$factory->define(Media::class, function (Faker $faker) {
    $name = $faker->name;

    return [
        'collection' => $faker->text,
        'name' => $name,
        'file_name' => $name . '.jpg',
    ];
});
