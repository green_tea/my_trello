<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\User\UserControllerService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function getMe()
    {
        /** @var User $user */
        $user = Auth::user();

        return $user->load('avatar');
    }

    public function updateMe()
    {
        /** @var User $user */
        $user = Auth::user();

        $validation_rules = [
            'name' => 'string|max:255',
            'second_name' => 'string|nullable|max:255',
            'username' => [
                Rule::unique('users', 'username')->whereNot('username', $user->username),
                'string',
                'max:255',
            ],
            'email' => [
                Rule::unique('users', 'email')->whereNot('email', $user->email),
                'string',
                'email',
                'max:255',
            ]
        ];

        $this->validate(request(), $validation_rules);

        $user = UserControllerService::updateUser($user, request()->only(array_keys($validation_rules)));

        return $user;
    }

    public function updateMyAvatar()
    {
        $this->validate(request(), [
            'image' => 'image|required|mimes:jpeg|max:10240' // 10 Mb
        ]);

        /** @var User $user */
        $user = Auth::user();

        $user->updateAvatar(request()->file('image'));

        return $user->refresh();
    }
}
