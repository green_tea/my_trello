<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class DeskController extends Controller
{
    public function recentlyViewed()
    {
        $this->validate(request(), [
            'limit' => 'integer|min:1',
        ]);

        $limit = request('limit', 4);

        /** @var User $user */
        $user = Auth::user();

        if (is_null($user)) {
            throw ValidationException::withMessages(['You are not authorize']);
        }

        return $user->desks()->orderBy('viewed_at', 'DESC')->limit($limit)->get();
    }
}
