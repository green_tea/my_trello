<?php

namespace App\Http\Middleware;

use Closure;

class Authentificated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (is_null($request->user())) {
            abort(401);
        }

        return $next($request);
    }
}
