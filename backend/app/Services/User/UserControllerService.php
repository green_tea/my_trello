<?php

namespace App\Services\User;

use App\Models\User;

class UserControllerService
{
    public static function updateUser(User $user, array $data) : User
    {
        $user->update($data);

        return $user->refresh();
    }
}
