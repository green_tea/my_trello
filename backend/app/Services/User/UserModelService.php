<?php


namespace App\Services\User;


use App\Models\Media;
use App\Models\User;
use Illuminate\Http\UploadedFile;

class UserModelService
{
    /** @var User $user */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }


    public function updateAvatar(UploadedFile $avatar) : Media
    {
        /** @var Media $avatar_model */
        $avatar_model = $this->user->avatar;

        if (!is_null($avatar_model)) {
            $avatar_model->deleteWithFile();
        }

        return Media::createFromFile($avatar, $this->user, 'avatar');
    }
}
