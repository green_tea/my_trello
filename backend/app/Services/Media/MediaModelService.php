<?php

namespace App\Services\Media;

use App\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class MediaModelService
{
    /** @var Media $media */
    protected $media;

    public function __construct(Media $media)
    {
        $this->media = $media;
    }

    public static function createFromFile(UploadedFile $file, Model $model, string $collection) : Media
    {
        $r = Storage::disk('local')->put($collection, $file);

        $name = pathinfo($r, PATHINFO_FILENAME);

        $media = new Media([
            'collection' => $collection,
            'name' => $name,
            'file_name' => $name . '.' . pathinfo($r, PATHINFO_EXTENSION),
        ]);

        $media->model()->associate($model);
        $media->save();

        return $media;
    }

    public function deleteWithFile()
    {
        Storage::disk('local')->delete($this->getPath());

        $this->media->delete();
    }

    protected function getPath() : string
    {
        return $this->media->collection . '/' . $this->media->file_name;
    }

}
