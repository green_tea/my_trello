<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property Carbon $viewed_at
 * @property Carbon $updated_at
 * @property Carbon $created_at
 */
class Desk extends Model
{
    protected $fillable = [
        'name',
        'viewed_at',
    ];

    public function users() {
        return $this->belongsToMany(User::class, 'subscriptions')->withPivot('role');
    }

    public function setViewed(bool $with_save = true)
    {
        $this->viewed_at = Carbon::now();

        if ($with_save) {
            $this->save();
        }
    }
}
