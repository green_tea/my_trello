<?php

namespace App\Models;

use App\Services\User\UserModelService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Validation\ValidationException;

/**
 * Class User
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $second_name
 * @property string $username
 * @property string $password
 * @property string $email
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'second_name', 'username', 'password', 'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function avatar() : MorphOne
    {
        return $this->morphOne(Media::class, 'model');
    }

    /** @var UserModelService|null */
    protected $service = null;

    protected function getService() : UserModelService
    {
        if (is_null($this->service)) {
            $this->service = new UserModelService($this);
        }

        return $this->service;
    }

    public function updateAvatar(UploadedFile $avatar)
    {
        $service = $this->getService();

        $service->updateAvatar($avatar);
    }

    public function desks()
    {
        return $this->belongsToMany(Desk::class, 'subscriptions')->withPivot('role');
    }

    public function authToDesks(array $desks)
    {
        $array_to_create = [];

        foreach ($desks as $desk_id => $role) {
            if (!Subscription::isValidType($role)) {
                throw ValidationException::withMessages(['role ' . $role . ' does not exist']);
            }

            $array_to_create[] = [
                'role' => $role,
                'desk_id' => $desk_id,
                'user_id' => $this->id,
            ];
        }

        Subscription::query()->insert($array_to_create);
    }
}
