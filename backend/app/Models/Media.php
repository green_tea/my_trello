<?php

namespace App\Models;

use App\Services\Media\MediaModelService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Http\UploadedFile;

/**
 * Class Media
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $second_name
 * @property string $file_name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Media extends Model
{
    protected $table = 'medias';

    protected $fillable = [
        'collection',
        'name',
        'file_name',
    ];

    /** @var null|MediaModelService */
    protected $service = null;

    protected function getService() : MediaModelService
    {
        if (is_null($this->service)) {
            $this->service = new MediaModelService($this);
        }

        return $this->service;
    }

    public function model() : MorphTo
    {
        return $this->morphTo();
    }

    public static function createFromFile(UploadedFile $file, Model $model, string $collection) : Media
    {
        return MediaModelService::createFromFile($file, $model, $collection);
    }

    public function deleteWithFile()
    {
        $this->getService()->deleteWithFile();
    }
}
