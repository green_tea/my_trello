<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Subscription extends Model
{
    protected $fillable = [
        'role',
    ];

    protected $primaryKey = ['user_id', 'desk_id'];

    public const ROLES = [
        'ADMIN' => 'ADMIN',
        'CREATOR' => 'CREATOR',
        'USER' => 'USER',
    ];

    public static function isValidType(string $type) : bool
    {
        return Arr::has(self::ROLES, $type);
    }
}
