<?php

namespace Tests\Feature\Desk;

use App\Models\Desk;
use Carbon\Carbon;
use Tests\Feature\User\UserTestCase;

class RecentlyViewedTest extends UserTestCase
{
    public function test_recently_viewed_check_limit()
    {
        $data = [
            'limit' => 2,
        ];

        $desks = factory(Desk::class, 3)->create();

        $this->user->authToDesks([
            $desks[0]->id => 'USER',
            $desks[1]->id => 'USER',
            $desks[2]->id => 'USER',
        ]);

        $this->get(route('client:desks:recently_viewed', $data))->assertJsonCount(2);
    }

    public function test_check_default_limit_value()
    {
        $desks = factory(Desk::class, 5)->create();

        $this->user->authToDesks([
            $desks[0]->id => 'USER',
            $desks[1]->id => 'USER',
            $desks[2]->id => 'USER',
            $desks[3]->id => 'USER',
            $desks[4]->id => 'USER',
        ]);

        $this->get(route('client:desks:recently_viewed'))->assertJsonCount(4);
    }

    public function test_check_user_filter()
    {
        $desks = factory(Desk::class, 3)->create();

        $this->user->authToDesks([
            $desks[0]->id => 'USER',
        ]);

        $this->get(route('client:desks:recently_viewed'))->assertJsonCount(1)
        ->assertJson([
            [
                'id' => $desks[0]->id,
            ]
        ]);
    }

    public function test_check_sorting_order()
    {
        /** @var Desk $desk1 */
        $desk1 = factory(Desk::class)->create([
            'viewed_at' => Carbon::now()->subDay(),
        ]);

        /** @var Desk $desk2 */
        $desk2 = factory(Desk::class)->create([
            'viewed_at' => Carbon::now()->subHour(),
        ]);

        $this->user->authToDesks([
            $desk1->id => 'USER',
            $desk2->id => 'USER',
        ]);

        $this->get(route('client:desks:recently_viewed'))->assertJsonCount(2)
            ->assertJson([
                [
                    'id' => $desk2->id,
                ],
                [
                    'id' => $desk1->id,
                ]
            ]);
    }
}
