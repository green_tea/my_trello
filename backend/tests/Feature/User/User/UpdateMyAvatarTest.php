<?php

namespace Tests\Feature\User\User;

use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\Feature\User\UserTestCase;

class UpdateMyAvatarTest extends UserTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        foreach (Storage::disk('local')->allFiles('avatar') as $file) {
            Storage::disk('local')->delete($file);
        }
    }

    public function test_update_my_avatar()
    {
        $image = UploadedFile::fake()->image('image.jpg');

        $data = [
            'image' => $image,
            '_token' => csrf_token(),
        ];

        $this->putJson(route('client:users:update_my_avatar'), $data)->assertSuccessful();

        /** @var User $user */
        $user = User::query()->first();

        $this->assertDatabaseHas('medias', [
            'collection' => 'avatar',
            'model_id' => $user->id,
            'model_type' => $user->getMorphClass(),
        ]);

        $file_name = $user->avatar->file_name;

        Storage::assertExists('avatar/' . $file_name);
    }

    public function test_check_not_correct_mime_type()
    {
        $image = UploadedFile::fake()->image('image.php');

        $data = [
            'image' => $image,
            '_token' => csrf_token(),
        ];

        $this->putJson(route('client:users:update_my_avatar'), $data)->assertJsonValidationErrors([
            'image' => 'The image must be a file of type: jpeg.',
        ]);
    }

    public function test_check_update_avatar_when_it_already_exist()
    {
        $image = UploadedFile::fake()->image('image.jpg');

        $data = [
            'image' => $image,
            '_token' => csrf_token(),
        ];

        $this->put(route('client:users:update_my_avatar'), $data)->json();

        /** @var User $user */
        $user = User::query()->first();

        $this->assertDatabaseHas('medias', [
            'collection' => 'avatar',
            'model_id' => $user->id,
            'model_type' => $user->getMorphClass(),
        ]);

        $file_name1 = $user->avatar->file_name;

        Storage::disk('local')->assertExists('avatar/' . $file_name1);

        $image = UploadedFile::fake()->image('image1.jpg');

        $data = [
            'image' => $image,
            '_token' => csrf_token(),
        ];

        $this->put(route('client:users:update_my_avatar'), $data)->json();

        /** @var User $user */
        $user = User::query()->first();

        $this->assertDatabaseHas('medias', [
            'collection' => 'avatar',
            'model_id' => $user->id,
            'model_type' => $user->getMorphClass(),
        ]);

        $file_name2 = $user->avatar->file_name;

        Storage::assertExists('avatar/' . $file_name2);
        Storage::assertMissing('avatar/' . $file_name1);
    }
}
