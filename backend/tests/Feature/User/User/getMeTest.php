<?php

namespace Tests\Feature\User\User;

use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class getMeTest extends TestCase
{
    public function test_check_not_auth_user()
    {
        $this->get(route('client:users:get_me'))->assertStatus(401);
    }

    public function test_check_get_me()
    {
        $user = factory(User::class)->create();
        Auth::login($user);

        $this->get(route('client:users:get_me'))->assertSuccessful()
            ->assertJson([
                'id' => $user->id,
            ]);
    }

    public function test_check_get_me_load_avatar()
    {
        $image = UploadedFile::fake()->image('image.jpg');

        /** @var User $user */
        $user = factory(User::class)->create();
        $user->updateAvatar($image);

        Auth::login($user);

        $this->get(route('client:users:get_me'))->assertJson([
            'avatar' => [
                'collection' => 'avatar',
                'file_name' => 'image.jpg',
                'name' => 'image',
                'model_type' => $user->getMorphClass(),
                'model_id' => $user->id,
            ],
        ]);
    }
}
