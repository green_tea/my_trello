<?php

namespace Tests\Feature\User\User;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Tests\Feature\User\UserTestCase;

class UpdateMeTest extends UserTestCase
{
    public function test_check_update_me()
    {
        $data = [
            'name' => 'kek',
            'second_name' => 'kek',
            'email' => 'kek@lalka.zu',
            'username' => 'keklolzu',
            '_token' => csrf_token(),
        ];

        $this->put(route('client:users:update_me'), $data);

        unset($data['_token']);

        $this->assertDatabaseHas('users', $data);
    }

    public function test_check_update_me_with_null_second_name()
    {
        $data = [
            'second_name' => null,
            '_token' => csrf_token(),
        ];

        $this->put(route('client:users:update_me'), $data);

        self::assertNull(User::query()->first()->second_name);
    }

    public function test_check_update_me_with_email_already_exist()
    {
        /** @var User $user */
        $user = factory(User::class)->create();

        $data = [
            'email' => $user->email,
            '_token' => csrf_token(),
        ];

        $this->putJson(route('client:users:update_me'), $data)->assertJsonValidationErrors([
            'email' => 'The email has already been taken.',
        ]);
    }

    public function test_check_update_me_with_username_already_exist()
    {
        /** @var User $user */
        $user = factory(User::class)->create();

        $data = [
            'username' => $user->username,
            '_token' => csrf_token(),
        ];

        $this->putJson(route('client:users:update_me'), $data)->assertJsonValidationErrors([
            'username' => 'The username has already been taken.',
        ]);
    }

    public function test_check_update_not_auth()
    {
        Auth::logout();

        $this->put(route('client:users:update_me'))->assertStatus(401);
    }
}
