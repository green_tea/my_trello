<?php

namespace Tests\Feature\Guest;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_check_login()
    {
        Session::start();

        /** @var User $user */
        $user = factory(User::class)->create();

        $data = [
            'username' => $user->username,
            'password' => 'secret',
            '_token' => csrf_token(),
        ];

        self::assertNull(Auth::user());

        $this->postJson(route('guest:login'), $data)->assertSuccessful();

        self::assertSame($user->id, Auth::user()->id);
    }

    public function test_check_user_login_with_invalid_password()
    {
        Session::start();

        /** @var User $user */
        $user = factory(User::class)->create();

        $data = [
            'username' => $user->username,
            'password' => 'kek',
            '_token' => csrf_token(),
        ];

        $this->postJson(route('guest:login'), $data)->assertJson([
            'errors' => [
                'username' => [
                    'These credentials do not match our records.',
                ]
            ]
        ]);
    }

    public function test_check_user_login_with_invalid_username()
    {
        Session::start();

        /** @var User $user */
        factory(User::class)->create();

        $data = [
            'username' => 'kek',
            'password' => 'secret',
            '_token' => csrf_token(),
        ];

        $this->postJson(route('guest:login'), $data)->assertJson([
            'errors' => [
                'username' => [
                    'These credentials do not match our records.',
                ]
            ]
        ]);
    }
}
