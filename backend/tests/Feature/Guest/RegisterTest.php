<?php

namespace Tests\Feature\Guest;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    public function test_check_register()
    {
        Session::start();

        $data = [
            'email' => 'kek@lol.zu',
            'username' => 'lalka',
            'password' => 'keklolzu',
            'password_confirmation' => 'keklolzu',
            'name' => 'Vlad',
            'second_name' => 'test',
            '_token' => csrf_token(),
        ];

        self::assertCount(0, User::all());

        $this->post(route('guest:register'), $data);

        self::assertCount(1, User::all());
    }

    public function test_check_register_with_email_already_in_use()
    {
        Session::start();

        factory(User::class)->create(['email' => 'kek@lol.zu']);

        $data = [
            'email' => 'kek@lol.zu',
            'username' => 'lalka',
            'password' => 'keklolzu',
            'password_confirmation' => 'keklolzu',
            'name' => 'Vlad',
            'second_name' => 'test',
            '_token' => csrf_token(),
        ];

        $this->postJson(route('guest:register'), $data)->assertJsonValidationErrors([
            'email' => 'The email has already been taken.',
        ]);
    }

    public function test_check_register_with_username_already_in_use()
    {
        Session::start();

        factory(User::class)->create(['username' => 'lalka']);

        $data = [
            'email' => 'kek@lol.zu',
            'username' => 'lalka',
            'password' => 'keklolzu',
            'password_confirmation' => 'keklolzu',
            'name' => 'Vlad',
            'second_name' => 'test',
            '_token' => csrf_token(),
        ];

        $this->postJson(route('guest:register'), $data)->assertJsonValidationErrors([
            'username' => 'The username has already been taken.',
        ]);
    }

    public function test_check_register_without_second_name()
    {
        Session::start();

        $data = [
            'email' => 'kek@lol.zu',
            'username' => 'lalka',
            'password' => 'keklolzu',
            'password_confirmation' => 'keklolzu',
            'name' => 'Vlad',
            '_token' => csrf_token(),
        ];

        $this->postJson(route('guest:register'), $data);

        self::assertCount(1, User::all());

        /** @var User $user */
        $user = User::first();

        self::assertNull($user->second_name);
    }
}
